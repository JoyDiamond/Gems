#   Copyright (c) 2018 Joy Diamond.  All rights reserved.

#
#   Maybe 'language pattern match' is better?
#
language pattern                                    #   Easier to read than 'language tremolite'

joy     := !/'Joy'
diamond := 'Diamond'
space   := ' '                                      #   Just a simple space
joy     := { Joy & space | more } + Diamond         #   me!
